const items = [];
const previewModal = new bootstrap.Modal(document.getElementById("modal"), {});
const alertError = document.getElementById("errorAlert");
const alertSuccess = document.getElementById("successAlert");
const previewContainer = document.getElementById("previewContainer");
const previewWarning = document.getElementById("previewWarning");
const tableBodyEl = document.querySelector("#dataTable tbody");

const onPreviewClick = (id, type) => {
  previewContainer.style.display = "block";
  previewWarning.style.display = "none";

  const item = items.find((f) => f.id === id);
  previewModal.show();

  if (item[type].type === "application/msword") {
    previewWarning.style.display = "block";
    previewContainer.style.display = "none";
    return;
  }

  previewContainer.src = URL.createObjectURL(item[type]);
};

const appendNewData = (item) => {
  const newId = items.length > 0 ? items.at(-1).id + 1 : 1;

  items.push({
    id: newId,
    ...item,
  });

  const newRowNode = document.createElement("tr");
  const newRowContent = `
     <td>${newId}</td>
     <td>${item.name}</td>
     <td>${item.address}</td>
     <td>${item.gender}</td>
     <td>${item.birthDate}</td> 
     <td><button class="btn btn-primary" onClick="onPreviewClick(${newId}, 'photo')"><i class="fas fa-eye"></i></button></td> 
     <td><button class="btn btn-primary" onClick="onPreviewClick(${newId}, 'cv')"><i class="fas fa-eye"></i></button></td> 
     <td><button class="btn btn-primary" onClick="onPreviewClick(${newId}, 'certificate')"><i class="fas fa-eye"></i></button></td> 
  `;

  newRowNode.innerHTML = newRowContent;

  tableBodyEl.appendChild(newRowNode);
  document.getElementById("registrationForm").reset();
};

const isFormValid = (formProps) => {
  for (const control in formProps) {
    if (formProps[control] instanceof File) {
      if (formProps[control].size === 0) return false;
    } else {
      if (!formProps[control]) return false;
    }
  }

  return true;
};

const showError = () => {
  alertSuccess.style.display = "none";
  alertError.style.display = "block";
  alertError.scrollIntoView();
};

const showSuccess = () => {
  alertError.style.display = "none";
  alertSuccess.style.display = "block";
  tableBodyEl.scrollIntoView();
};

const resetAlert = () => {
  alertError.style.display = "none";
  alertSuccess.style.display = "none";
};

document.getElementById("resetButton").addEventListener("click", (e) => {
  resetAlert();
});

document.getElementById("registrationForm").addEventListener("submit", (e) => {
  event.preventDefault();

  const formData = new FormData(e.target);
  const formProps = Object.fromEntries(formData);

  const isValid = isFormValid(formProps);
  if (isValid) {
    appendNewData(formProps);
    showSuccess();
  } else {
    showError();
  }
});
